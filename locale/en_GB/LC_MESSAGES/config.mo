��    S      �  q   L             %        ;     J     X     i          �  
   �     �     �  	   �     �     �     �          &     +     9     H     \     e     u     �     �     �     �     �     �  
   �     �      	     	     4	     J	     g	  
   �	     �	     �	     �	     �	     �	     �	     �	     �	     

     &
     6
     E
     U
     f
     s
     �
     �
     �
     �
     �
     �
     �
     
          ,     :     F     Y     j     s     �     �     �     �     �     �  
   �     �     �          !     *  
   ;     F     Y    g     x  %   �     �     �     �  B   �     5     B  
   Y     d     t     {       Q   �     �  �   �     �     �     �  �   �     ~     �  /   �     �     �     �     �  W     '   h  
   �     �     �  �   �     �     �  �   �  
   @     K     N     g  �   w  
        '     C     R      b     �     �     �  '   �     �  4   �     -     H     X     ^     n  @   t     �     �  0   �                0  �   >     �     �    	               ,  
   2     =  
   M     X     e     u     �  {   �          "     5            +       P   5          D          L                /   K                ,       <      E          !           %   $   @       N      J   I      G   *   B   &   =       7   R   1      ?       0              '   2          -   .                           O   F                 9       >   3          	       Q   H   8       6           )      ;   M       S   #                 A   :   (       "   
   4      C       404 There was an error updating mirr.OS:  add new module back to setup before you start before you start text choose wlan config module's width connecting connectionType debug action debug all debug infos title describe problem describe your problem describe your problem text done edit settings email not sent email not sent text ethernet export settings factory setting text factory settings failed field required file already exists file crypted file not moved full width generate debug info generate debug info button generate debug info text generate your logfile generate your logfile button generate your logfile text half width hi import existing settings import settings import settings text import submit input password invisible ssid layout settings mirr.OS sucessfully updated mirros settings module deleted module overview no file selected not uploaded only mirros backup file please fill all field report preview mirros problem title reload frontend reset mirros button reset mirros text restart mirros restore mirros restored from file save settings send report send report button send report text settings setup complete setup complete text setup mirros show debug info sorry thank you report troubleshooting visibility visible ssid what is your first name? where are you living? wireless wlan failed text your email your email address your language Project-Id-Version: mirr.OS 0.7.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-06-18 12:38+0200
PO-Revision-Date: 2018-06-18 12:38+0200
Last-Translator: Tobias Grasse <tg@glancr.de>
Language-Team: glancr team – kontakt@glancr.de
Language: en_GB
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-SourceCharset: UTF-8
X-Generator: Poedit 2.0.8
X-Poedit-Basepath: ../../..
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: modules
 404 - Website not found! There was an error updating mirr.OS:  Add new module Back to configuration page Before you start Please try if one of the options below might resolve your problem. Choose Wi-Fi Config module's width. Connecting connection type Action All Debugging Informations Please describe your problem as good as possible so that we are able to help you. Describe your problem If you experience problems with mirr.OS or with modules you can use this feature to inform us. To respond faster to your request we also a get a copy of your logfiles. Done! Edit settings The email could not be sent. There is a problem with sending an email. To solve this problem mirr.OS now creates a new AP (Access Point). When done, you need to configure your smart mirror again. Cable Export settings Your glancr has been reset to factory settings. Factory settings Something is wrong. field required The file already exists The provided file is encrypted. Please use a mirr.OS backup file which is not encrypted There was an error uploading your file. Full width Generate debug infos Generate informations The debug infos shows us how your mirr.OS is configured. For privacy reasons we anonymize your data as far as possible. This means: We do not receive passwords, tokens or other registration data. Generate your logfile Generate logfile The Apache logfile contains informations about the occurred errors. The logfile will be generated automatically if you click the button below. Half width Hi Import existing settings Import settings You can import your existing settings here. Please note that only the settings will be imported (tokens, keys, passwords). You need to install the modules manually. Import now Please enter your password. Invisible SSID Layout settings mirr.OS was successfully updated mirr.OS settings Module overview Module overview No file selected. Please select a file. Your file was not uploaded. Sorry, only JSON mirr.OS backup files are supported. Please fill in all fields. mirr.OS preview Title Reload frontend reset You can reset your mirr.OS installation to factory settings here Restart mirr.OS restore mirr.OS from backup The settings have been restored from this file:  save settings Transfer report Submit report If you send us your problem report, we will check if it is a general problem and create an update if necessary. You will get information about the status of your problem report. Settings Setup complete! mirr.OS ist now trying to connect to your Wi-Fi. Soon you will receive an email containing links for further configuration. The internal Wi-Fi GlancrAP will shut down now. Please reconnect with your personal Wi-Fi. Please note the output of your smart mirror. Set up mirr.OS Show debug info Sorry Thank you. Troubleshooting visibility Visible SSID Your first name Please enter your city. Wi-Fi Please reconnect your phone or laptop with the Wi-Fi GlancrAP and check if you entered the correct Wi-Fi name and password. your email address your email address your language 