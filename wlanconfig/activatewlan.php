<?php
include('../config/glancrConfig.php');

$firstname = getConfigValue("firstname");
$email = getConfigValue("email");
$city = getConfigValue("city");
$setup = @$_GET["setup"];
$lang = getConfigValue("language");

if ($firstname == "" || $email == "" || $city == "") {
    header("Location: /config/config.php?setup=true&lang=" . $lang);
    exit;
}

$connectionType = getConfigValue("connectionType");

if ($connectionType === 'wlan') {
    $ssid = getConfigValue("wlanName");
    $pass = getConfigValue("wlanPass");
    $params = escapeshellarg($ssid) . ' ' . escapeshellarg($pass);
} else {
    $params = '';
}
exec('sudo /home/pi/activatewlan.sh ' . $params, $status);

if (!$status[0]) {
    echo " failed";
    sleep(5);
    header("Location: index.php");
    exit;
} else {
    $host = 'glancr.net';
    $port = 80;
    $waitTimeoutInSeconds = 1;
    $pingTries = 0;
    while (!$fp = fsockopen($host, $port, $errCode, $errStr, $waitTimeoutInSeconds)) {
        usleep(1000000);
        $pingTries++;
        if ($pingTries == 20) {
            echo " failed";
            sleep(5);
            header("Location: index.php");
            exit;
        }
    }
    fclose($fp);

    exec('ip -f inet -o addr show ' . $connectionType . '0|cut -d\  -f 7 | cut -d/ -f 1', $ip);

    // send email
    // @FIXME This does still not use the GlancrServerApi class, thus the code diverted. When refactoring, pay attention to two extra commands here: On mail send error, connection type is reset to WLAN and reset.sh + redirect are triggered. At least resetting the connection type can be omitted, since reset.sh overwrites all config with defaults anyway (which includes setting connectionType === "wlan").
    $url = 'https://api.glancr.de/mail/';

    $ch = curl_init($url);

    $jsonData = array(
        'name' => $firstname,
        'email' => $email,
        'localip' => $ip[0],
        'type' => $setup === "true" ? 'setup' : 'change',
        'language' => $lang
    );

    $jsonDataEncoded = json_encode($jsonData);

    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

    $result = curl_exec($ch);

    $mailTries = 0;
    while (curl_getinfo($ch, CURLINFO_HTTP_CODE) != 200) {
        $result = curl_exec($ch);
        $mailTries++;
        usleep(5000000);
        if ($mailTries == 20) {
            setConfigValue('connectionType', 'wlan');
            setConfigValue('emailNotSent', '1');

            exec('sudo /home/pi/resetwlan.sh', $status);
            header("Location: index.php");
            exit;
        }
    }

    // save data
    setConfigValue('ip', $ip[0]);
    setConfigValue('reload', "1");

    header("Location: ../config/");
}
