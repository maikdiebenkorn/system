#!/usr/bin/env bash

[[ $CI_BUILD_TAG =~ "beta" ]] && CHANNEL="beta" || CHANNEL="stable"

curl -O https://api.glancr.de/update/system/update.json.$CHANNEL
php -r '$updateFile = json_decode(file_get_contents("update.json." . $argv[1]), TRUE); $updateFile[$argv[2]] = "https://api.glancr.de/update/system/" . $argv[2] . ".zip"; file_put_contents("update.json." . $argv[1], json_encode($updateFile, JSON_PRETTY_PRINT));' -- "$CHANNEL" "$CI_BUILD_TAG"
rsync -t "$CI_BUILD_TAG".zip update.json.$CHANNEL $USER@api.glancr.de:$ROOT/

if [[ $CHANNEL == "stable" ]]; then
    curl -O https://api.glancr.de/update/system/update.json.beta
    php -r '$updateFile = json_decode(file_get_contents("update.json.beta"), TRUE); $updateFile[$argv[2]] = "https://api.glancr.de/update/system/" . $argv[2] . ".zip"; file_put_contents("update.json.beta", json_encode($updateFile, JSON_PRETTY_PRINT));' -- "beta" "$CI_BUILD_TAG"
    rsync -t "$CI_BUILD_TAG".zip update.json.beta $USER@api.glancr.de:$ROOT/
fi
