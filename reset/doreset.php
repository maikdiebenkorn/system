<?php
use glancr\GlancrServerApi;

include('../config/glancrConfig.php');
include GLANCR_ROOT . "/classes/GlancrServerApi.php";

$api = new GlancrServerApi(GLANCR_API_BASE);
$api->triggerMail('reset');

include GLANCR_ROOT . "/includes/db-connector.php";

$current_branch = getConfigValue('branch');

$result = $conn->query('TRUNCATE TABLE configuration');

setConfigValue('ip', '');
setConfigValue('firstname', '');
setConfigValue('branch', $current_branch);
setConfigValue('city', '');
setConfigValue('email', '');
setConfigValue('wlanInvisible', '0');
setConfigValue('wlanName', '');
setConfigValue('wlanPass', '');
setConfigValue('connectionType', 'wlan');
setConfigValue('owmApiKey', '');
setConfigValue('language', 'de_DE');
setConfigValue('reload', '0');
setConfigValue('emailNotSent', '0');
setConfigValue('timeformat', '24');

unlink(GLANCR_ROOT . '/update.log');

exec('sudo /home/pi/reset.sh', $status);
