<link rel="stylesheet" href="../config/css/font-awesome.min.css">

<style media="screen">

    .site__title ul {
        margin-bottom: 0;
    }

    .site__title .left img {
        margin-top: 0;
    }

	.fa {
		margin-right: 5px;
	}

	.icon {
		width: 60px !important;
		height: 60px !important;
		margin-top: 0px !important;
        display: inline;
	}

    .icon.active svg {
        fill: #7993b3;
    }

	@media screen and (min-width: 0em) and (max-width: 440px) {

        .site__title .item {
            margin-left: 15px;
        }

	    .icon {
	        height: 40px !important;
	        width: 40px !important;
			margin-top: 20px !important;
		}

        li.item {
            margin-top: -5px !important;
        }
	}

main {
	padding-top: 2rem;
}

</style>

<header class="expanded row">
	<div class="small-12 columns site__title">
		<div class="row">

			<ul class="left">
				<li class="item">
                    <img src="assets/glancr_logo.png" width="57" height="30" alt="GLANCR Logo" srcset="/config/assets/glancr_logo.png 57w, /config/assets/glancr_logo@2x.png 114w, /config/assets/glancr_logo@2x.png 171w">
				</li>
			</ul>

			<?php
                if(!isset($setup)) $setup = false;
				if (!$setup) {
					?>
						<ul class="right" id="nav">
                            <li class="item"><a href="/config/report.php" class="icon has-tip" data-tooltip aria-has-popup="true" title="<?php echo _('troubleshooting'); ?>"><?php echo file_get_contents("../config/assets/nav/help.svg"); ?></a></li>
							<li class="item"><a href="/glancr" target="_blank" class="icon has-tip" data-tooltip title="<?php echo _('preview mirros'); ?>"><?php echo file_get_contents("../config/assets/nav/preview.svg"); ?></a></li>
							<li class="item"><a href="/config/config.php" class="icon has-tip" data-tooltip title="<?php echo _('mirros settings'); ?>"><?php echo file_get_contents("../config/assets/nav/settings.svg"); ?></a></li>
                            <li class="item"><a href="/config/" class="icon has-tip" data-tooltip title="<?php echo _('layout settings'); ?>"><?php echo file_get_contents("../config/assets/nav/layout.svg"); ?></a></li>
						</ul>
					<?php
				}
			?>
		</div>
	</div>
</header>

<script type="text/javascript">
	$(function () {

        let navLinks = $('#nav li a')

        navLinks.each(function () {
            let navLink = $(this)
            if (navLink.attr('href') === window.location.pathname) {
                navLink.addClass('active')
            }
        })
	});

	function isTouchDevice(){
        return true == ("ontouchstart" in window || window.DocumentTouch && document instanceof DocumentTouch);
	}

	function reloadMirror(){
		$.post('/config/setConfigValueAjax.php', {'key': 'reload', 'value': '1'}).success(function() {
			$('#ok').show(30, function() {
				$(this).hide('slow');
			});
		}).fail(function() {
			$('#error').show(30, function() {
				$(this).hide('slow');
			});
		});
		return false;
	}
</script>
