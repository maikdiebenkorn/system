<?php
include('../config/glancrConfig.php');

$language = getConfigValue('language');

putenv("LANG=$language");
setlocale(LC_ALL, $language . '.utf8');

setGetTextDomain('config', GLANCR_ROOT ."/locale");
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
  <title>404</title>
  <link rel="stylesheet" type="text/css" href="../config/css/main.css">
  <script type="text/javascript" src="../config/bower_components/jquery/dist/jquery.min.js"></script>
  <script type="text/javascript" src="../config/bower_components/foundation-sites/dist/foundation.min.js"></script>
  <?php include "../includes/favicons.php"; ?>
</head>
<body>

  <?php include "../includes/navigation.php"; ?>


<!-- Container für den Extra-Rand -->
<main class="container">
    <section class="row">
        <div class="small-12 columns">
            <p class="instruction__stepper"><?php echo _('sorry');?>,</p>
            <h2 class="instruction__title"><?php echo _('404');?></h2>
            <p class="instruction__text">
                <a href="../config/"><?php echo _('back to setup');?></a>
            </p>

 		</div>
    </section>
</main>

</body>
</html>
