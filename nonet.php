<?php
include('config/glancrConfig.php');

$language = getConfigValue('language');

putenv("LANG=$language");
setlocale(LC_ALL, $language . '.utf8');

setGetTextDomain('config', GLANCR_ROOT ."/locale");
?>
<!DOCTYPE html>
<html id="language" lang="en_GB">
<head>
  <title>Glancr Start</title>
  <link rel="stylesheet" type="text/css" href="glancr/css/fonts.css">
  <style>
      body {
          background-color: black;
          color: white;
          font-size: 200%;
          font-family: 'Alegreya Sans';
      }

      main {
          height: 90vh;
          text-align: center;
          display: flex;
          align-items: center;
          justify-content: center;
      }
  </style>
</head>

<body>
  <main>
    <article>
<?php
$connectionType = getConfigValue('connectionType');

exec('ip -f inet -o addr show ' . $connectionType . '0|cut -d\  -f 7 | cut -d/ -f 1', $ip);

$noIp = false;

if(is_array($ip)) {
  if(array_key_exists(0, $ip)) {
    if($ip[0] == '') {
      $noIp = true;
    }
  } else {
    $noIp = true;
  }
} else {
  $noIp = true;
}

if($noIp) {
  echo '<img src="setup/assets/connecting.svg" />';
  echo '<h2>' . _('connecting') .'</h2>';

  if(isset($_GET['tryTime'])) {
    $time = $_GET['tryTime'];
  } else {
    $time = time();
  }
  $sleep = 1;
  $url = 'nonet.php?tryTime=' . $time;
} else if($ip[0] != '192.168.8.1') { // We have an IP and it's not the AP mode.
  echo '<img src="setup/assets/email.svg" />';
  echo '<h2>' . _('Done. Now, check your mails.') . '</h2>';
  $sleep = 15;
  $url = 'glancr/index.php';
} else {
  if(isset($_GET['tryTime'])) {
    $reset = getConfigValue('reset');
    $emailNotSent = getConfigValue('emailNotSent');
    if($reset) { // A reset was requested, show this info.
      echo '<img src="setup/assets/error.svg" />';
      echo '<h2>' . _('factory settings') . '!</h2><p>' . _('factory setting text') . '</p>';

      setConfigValue('reset', '0');
    } else if($emailNotSent) { // The email could not be sent, something's wrong with the connection.
      echo '<img src="setup/assets/error.svg" />';
      echo '<h2>' . _('email not sent') . '!</h2><p>' . _('email not sent text') . '</p>';

      setConfigValue('emailNotSent', '0');

    } else {
      echo '<h2>' . _('failed') . '!</h2><p>' . _('wlan failed text') . '</p>';
    }
    $sleep = 15;
    $url = 'nonet.php';
  } else {
    $sleep = 1;
    $url = 'setup/';
  }
}

?>
    </article>
  </main>
<script>
setTimeout(function(){location.href= '<?php echo $url; ?>'}, <?php echo $sleep * 1000; ?>);
</script>

</body>
</html>
