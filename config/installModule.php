<?php
include('glancrConfig.php');

//$orderZip = file('http://localhost/server/zipModule.php?module=' . $_POST['name']);
//$file = 'http://localhost/server/moduleZips/' . $_POST['name'] . '-' . $_POST['version'] . '.zip';
$file = $_FILES['moduleZip']['tmp_name'];
$newFile = GLANCR_ROOT . '/tmp/tmp_file.zip';

if (!copy($file, $newFile)) {
	echo "failed to copy $file...\n";
	return;
}

$zip = new ZipArchive();
if ($zip->open($newFile, ZIPARCHIVE::CREATE)) {
	$zip->extractTo(GLANCR_ROOT .'/modules/');

    //@TODO This can be removed; PHP correctly sets 0755 for directories and 0644 for files
//	$iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator(GLANCR_ROOT .'/modules/' . $_FILES['moduleZip']['name']));
//
//	foreach($iterator as $item) {
//		chmod($item, 0777);
//	}
	$moduleName = substr($zip->getNameIndex(0), 0 , -1);
	$zip->close();
	$modules_enabled = file(GLANCR_ROOT .'/config/modules_enabled');
	if($_GET['col'] == 1) {
		$modules_enabled[$_GET['row']] = substr($modules_enabled[$_GET['row']], 0, -1) . $moduleName . "\n";
	} else {
		$modules_enabled[$_GET['row']] = $moduleName . substr($modules_enabled[$_GET['row']], 0, -1) . "\n";
        }
	$fp = fopen(GLANCR_ROOT .'/config/modules_enabled', 'w');
	fwrite($fp, implode("", $modules_enabled));

	echo 'ok';
} else {
	echo 'Fehler';
	
}

unlink($newFile);
setConfigValue('reload', '1');
?>
