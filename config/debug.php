<?php

  include('glancrConfig.php');

  $language = getConfigValue('language');
  $info_file = file_get_contents('../info.json');

  putenv("LANG=$language");
  setlocale(LC_ALL, "$language.UTF-8");
  bindtextdomain('config', GLANCR_ROOT ."/locale");
  textdomain('config');
  bind_textdomain_codeset('config', 'UTF-8');

?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
  <title>Debugging Informations</title>
  <link rel="stylesheet" type="text/css" href="../config/css/main.css">
  <link rel="stylesheet" href="../config/bower_components/foundation-icon-fonts/foundation-icons.css" media="screen" title="no title" charset="utf-8">
  <script src="http://code.jquery.com/jquery-2.1.0.min.js"></script>

  <?php include "../includes/favicons.php"; ?>

  <style type="text/css">

    .debug-buttons {
      max-width: 1000px;
      margin-left: auto;
      margin-right: auto;
      text-align: center;
    }

    table {
      margin-top: 60px;
      color: black;
      table-layout: fixed;
      width: 100%;
    }

    .instruction__title {
      text-align: center;
    }

    table td {
      word-wrap: break-word;
      overflow-wrap: break-word;
    }

    table th {
      text-align: left;
    }

    table th:first-child {
      width: 30%;
    }

    table th:last-child {
      width: 85px;
      text-align: center;
    }

    table tr:hover {
      background-color: #ccc;
    }

  </style>
</head>
<body>

  <?php include "../includes/navigation.php"; ?>

  <main class="container">
    <div class="row">
      <div class="small-12 columns">
        <h2 class="instruction__title"><?php echo _("debug infos title"); ?></h2>
        <p>mirr.OS version: <?php echo json_decode($info_file)->version; ?><br>PHP version: <?php echo phpversion(); ?></p>

        <?php

          $keys_to_hide = [
            "wlanPass",
        		"owmApiKey",
        		"netatmo_access_token",
        		"netatmo_client_id",
        		"netatmo_client_secret",
        		"netatmo_refresh_token",
        		"todoist_api_token",
        		"tanken_key",
        		"twitch_access_token",
        		"wunderlist_access_token",
        		"fetchsql_passwd",
        		"withings_consumer_key",
        		"withings_consumer_secret",
        		"withings_request_token_key",
        		"withings_request_token_secret",
        		"withings_access_key",
        		"withings_access_secret",
        		"withings_token",
        		"withings_test_token",
        		"soundcloud-info_clientkey"
          ];

          $keys = array();
          @$key_compare = $_GET["key"];

          include "../includes/db-connector.php";

          if ($result = $conn->query("SELECT * FROM configuration;")) {

            echo "<table>";
            echo "<tr><th>Key</th><th>Value</th><th>" . _("debug action") . "</th></tr>";

            while ($row = $result->fetch_assoc()) {
              $parts = explode("_", $row["key"]);

              if (sizeof($parts) >= 2) {
                array_push($keys, $parts[0]);
              }

              if ($key_compare == null || $key_compare == $parts[0]) {

                $lock_icon = "";
                $field_type = "text";
                if (in_array($row["key"], $keys_to_hide)){
                  $lock_icon = "<i class='fi-lock' style='font-size: 13pt; margin-right: 5px;'></i>";
                  $field_type = "password";
                }

                echo "<tr>";
                echo "<td>";
                echo $lock_icon . $row["key"];

                if ($field_type == "password"){
                  echo "<a href='#' onclick=\"return showKey('".$row["key"]."')\"> (Show Key)</a>";
                }
                echo "</td>";
                echo "<td><input type='$field_type' id='" . $row["key"] . "' value='" . $row["value"] . "'></td>";
                echo "<td><a href='#' id='" . $row["key"] . "_btn' class='button' onclick=\"return saveValue('" . $row["key"] . "')\" style='width: 43px; display: none'><i class='fa fa-save'></i></a></td>";
                echo "</tr>";
              }
            }
            $result->free();

            $keys = array_unique($keys);

            echo "<div class='debug-buttons'>";
            echo "<a href='/config/debug.php' class='button'>" . _("debug all") . "</a>";

            foreach ($keys as $key => $value) {
              echo "<a href='/config/debug.php?key=$value' class='button'>$value</a>";
            }

            echo "</div>";
            echo "</table>";

          }
        ?>
      </div>
    </div>
  </main>

  <script>

  $("td input").keydown(function(event) {
    button = "#"+$(this).attr("id")+"_btn";
    $(button).show();
  });

  function saveValue(key){
    selector = "#" + key;
    $.post("/config/setConfigValueAjax.php", {"key": key, "value": $(selector).val()}).success(function(){
      $('#ok').show(30, function() {
				$(this).hide('slow');
			});
      button = "#"+key+"_btn";
      $(button).hide();
    }).fail(function(){
      $('#error').show(30, function() {
        $(this).hide('slow');
      });
    });
    return false;
  }

  function showKey(key){
    selector = "#" + key;
    type = $(selector).attr("type");

    if (type == "password"){
      $(selector).attr("type", "text");
      $(selector).parent().prev().children()[1].textContent = " (Hide Key)";
    } else {
      $(selector).attr("type", "password");
      $(selector).parent().prev().children()[1].textContent = " (Show Key)";
    }
    return false;
  }

  </script>

  <img style="z-index:1006; display: none; top: 300px; position: fixed; left:50%; margin-left: -150px;" id="ok" src="img/OK.png" alt="OK"/>
  <img style="z-index:1006; display: none; top: 300px; position: fixed; left:50%; margin-left: -150px;" id="error" src="img/ERROR.png" alt="ERROR"/>

</body>
</html>
