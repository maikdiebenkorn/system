<?php
include('glancrConfig.php');

$language = getConfigValue('language');
$firstname = getConfigValue('firstname');

putenv("LANG=$language");
setlocale(LC_ALL, "$language.UTF-8");
bindtextdomain('config', GLANCR_ROOT ."/locale");
textdomain('config');
bind_textdomain_codeset('config', 'UTF-8');


$modules_content = scandir(GLANCR_ROOT .'/modules');
$modules_available = [];

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
	<title>Problembehebung</title>
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" href="bower_components/foundation-icon-fonts/foundation-icons.css" media="screen" title="no title" charset="utf-8">
	<script src="http://code.jquery.com/jquery-2.1.0.min.js"></script>

	<?php include "../includes/favicons.php"; ?>

	<style type="text/css">
	.error {
		border: 2px solid red;
	}
	.validate {
		position: relative;
		top: -19px;
		font-size: small;
		color: #999;
		left: 2px;
		display: none;
	}

	.status-text {
		color: white;
		font-size: 15pt;
		display: none;
		margin-left: 20px;
		margin-top: -10px;
	}

    label {
      color: white;
    }

	</style>
	<script type="text/javascript" src="bower_components/jquery/dist/jquery.min.js"></script>
	<script type="text/javascript" src="js/animateRotate.js"></script>
</head>
<body>

	<?php include "../includes/navigation.php"; ?>

	<main class="container">
		<section>
			<div class="row">
				<div class="small-12 columns">
					<form>
						<h2 class="instruction__title"><?php echo _("troubleshooting"); ?></h2>
						<h3 class="instruction__title"><?php echo _("before you start"); ?></h3>
						<p class="instruction__text"><?php echo _("before you start text"); ?></p>

						<a href="#" class="button" onclick="return reloadMirror()" style="width: 32%;  margin-right: 1%"><i class="fa fa-refresh"></i> <?php echo _("reload frontend"); ?></a>
						<a href="#" class="button" onclick="$.get('/config/reboot.php').success(function(){ alert('Reboot')})" style="width: 32%; margin-right: 1%"><i class="fa fa-undo"></i> <?php echo _("restart mirros"); ?></a>
						<a href="/config/debug.php" class="button" style="width: 32%;"><i class="fa fa-info-circle"></i> <?php echo _("show debug info"); ?></a>

						<br><hr>
            <h3><?php echo _("Privacy notice"); ?></h3>
            <p><?php echo _("The information provided in this form will be processed to determine technical difficulties with your installation of mirr.OS. For further details on the data we collect and how it is processed, refer to our privacy statement at ") . dataProtectionStatementURL($link = true); ?></p>
            <label for="privacy-consent"><input type="checkbox" required aria-required="true" name="privacy-consent" id="privacy-consent" /><?php echo _("I agree"); ?></label>
            <hr>

						<h3 class="instruction__title">1. <?php echo _("describe your problem"); ?></h3>
						<p class="instruction__text"><?php echo _("describe your problem text"); ?></p>

						<input type="text" id="title" name="title" placeholder="<?php echo _('problem title'); ?> (<?php echo _('field required'); ?>)">

						<textarea rows="7" id="description" name="description" placeholder="<?php echo _('describe problem'); ?> (<?php echo _('field required'); ?>)"></textarea>
						<input type="email" id="email" name="email" placeholder="<?php echo _('your email'); ?> (<?php echo _('field required'); ?>)" value="<?php echo getConfigValue('email'); ?>"><br>

						<h3 class="instruction__title">2. <?php echo _('generate your logfile'); ?></h3>

						<p class="instruction__text"><?php echo _('generate your logfile text'); ?></p>
						<a href="#" class="button" onclick="return getLogfiles()"><i class="fa fa-file-code-o"></i> <?php echo _('generate your logfile button'); ?></a>

						<textarea rows="7" id="logfile" name="logfile" placeholder="(<?php echo _('field required'); ?>)"></textarea><br>

                        <h3 class="instruction__title">3. <?php echo _('generate your update logfile'); ?></h3>

                        <p class="instruction__text"><?php echo _('Are you having problems with updating mirr.OS or modules? This logfile contains additional information about the update process which might help us solve your issue. It will be appended to your problem report.'); ?></p>
                        <a href="#" class="button" onclick="return getUpdateLogfile()"><i class="fa fa-file-code-o"></i> <?php echo _('generate your logfile button'); ?></a>

                        <textarea rows="7" id="update_logfile" name="update_logfile" placeholder="(<?php echo _('field required'); ?>)"></textarea><br>

						<h3 class="instruction__title">4. <?php echo _('generate debug info'); ?></h3>
						<p class="instruction__text"><?php echo _('generate debug info text'); ?></p>
						<a href="#" class="button" onclick="return getDebuggingInfo()"><i class="fa fa-file-code-o"></i> <?php echo _('generate debug info button'); ?></a>

						<textarea rows="7" id="debugging_info" name="debugging_info" placeholder="(<?php echo _('field required'); ?>)"></textarea><br>

						<h3 class="instruction__title">5. <?php echo _('send report'); ?></h3>
						<p class="instruction__text"><?php echo _('send report text'); ?></p>

						<button href="#" onclick="return submitForm()" class="button" style="margin-top: 15px;"><i class="fa fa-send"></i> <?php echo _('send report button'); ?></button>

						<span id="success-status" class="status-text"><?php echo _('thank you report'); ?></span>
						<span id="invalid-status" class="status-text"><?php echo _('please fill all field report'); ?></span>
						<span id="error-status" class="status-text"><?php echo _('error while transmitting your report'); ?></span>

					</form>
				</div>
			</div>
		</section>
	</main>

	<script type="text/javascript">
		function getDebuggingInfo() {
			$.getJSON("/config/export_config.php?crypted=true").done(function(data) {
				$("#debugging_info").text(JSON.stringify(data, null, 2));
			});
			return false;
		}

		function getLogfiles() {
			$.get("/config/apache_log.php").done(function(data) {
				$("#logfile").text(data);
			});
			return false;
		}

		function getUpdateLogfile() {
            $.get("/config/update_log.php").done(function(data) {
                $("#update_logfile").text(data);
            });
            return false;
        }

		function submitForm() {
			$(".status-text").hide();

			privacy_consent = $('#privacy-consent')[0].checked

			title = $("#title").val();
			description = $("#description").val();
			email = $("#email").val();
			logfile = $("#logfile").val();
			logfile = $("#debugging_info").val();

			if (privacy_consent === false || title === "" || description === "" || email === "" || logfile === "" || logfile === "" ){
				$("#invalid-status").show();
				$("#error").show(30, function() {
					$(this).hide("slow");
				});
			} else {

				$.post("https://api.glancr.de/reports/new.php", $("form").serialize()).success(function(){
					$("#success-status").show();
					$("#ok").show(30, function() {
						$(this).hide("slow");
					});

					$("#title").val("");
					$("#description").val("");
					$("#email").val("");
					$("#logfile").val("");
                    $("#update_logfile").val("");
					$("#debugging_info").val("");

				}).fail(function() {
					$("#error-status").show();
					$("#error").show(30, function() {
						$(this).hide("slow");
					});
			  });
			}
			return false;
		}
	</script>
</body>
</html>
