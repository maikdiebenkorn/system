<?php
/**
 * Retrieves the last 20 lines of the update.log
 */

$log = file('../update.log');

if (!empty($log)) {
    $log = array_reverse($log);
    print(implode("", array_slice($log, 0, 50)));
} else {
    print("No update log messages on WARNING or higher yet");
}

