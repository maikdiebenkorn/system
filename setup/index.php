<!DOCTYPE html>
<html id="language" lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Glancr setup</title>

    <script src="../glancr/js/lib/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../glancr/css/fonts.css">
    <style>
        body {
            background-color: black;
            color: white;
            font-size: 200%;
            font-family: 'Alegreya Sans';
            overflow: hidden;
            padding: 7rem;
        }

        section {
            text-align: center;
        }

        hr {
            max-width: 6rem;
            margin: 6rem auto 6rem auto;
            transform: rotate(90deg);
        }

        table {
            margin: 0 auto;
            border-spacing: 0.125rem 0.625rem;
            text-align: center;
        }

        td {
            padding: 0.5rem;
            background: white;
            color: black;
            border-radius: 5px 0 0 5px;
        }

        td:nth-of-type(2) {
            border-radius: 0 5px 5px 0;
        }

        .smaller {
            font-size: smaller;
        }

        .margin-top {
            margin-top: 2rem;
        }
    </style>
</head>
<body>
<section class="instructions" id="wifi">
    <img src="../ap-qr.png" width="250" height="250" alt="">
    <br><em data-locale="1-1">or scan me</em>
    <p><span data-locale="1-2">I made a WLAN for you.</span><br><span data-locale="1-3">Connect your smartphone or laptop with me.</span>
    </p>
    <table>
        <tr>
            <td><code>Wi-Fi</code></td>
            <td><code>GlancrAP</code></td>
        </tr>
        <tr>
            <td><code>password</code></td>
            <td><code><?php echo htmlspecialchars(trim(file_get_contents('../appass.txt'))); ?></code></td>
        </tr>
    </table>
    <p class="smaller" id="connected" style="display: none"><span data-locale="1-5">Connected</span>: <span
            id="connected-device"></span></p>

    <img src="assets/connecting.svg" width="75px" class="margin-top"/>
</section>


<section class="instructions" style="display: none" id="browser">
    <img src="assets/http.svg" width="250" height="auto" alt="">
    <p>
        <span data-locale="2-1">On most devices, the setup screen should start automatically.</span>
        <br>
        <em data-locale="2-2">If it doesn't, visit http://glancr.conf in your browser.</em>
    </p>

    <img src="assets/connecting.svg" width="75px" class="margin-top"/>
</section>

<section class="instructions" style="display: none" id="follow">
    <img src="assets/instructions.svg" width="250" height="auto" alt="">
    <p data-locale="3-1">Follow the instructions to complete the setup!</p>
</section>

<script type="text/javascript">

    // Change the global document language indicator every 5 seconds.
    function changeLocale () {
        let langControl = document.getElementById('language')
        languages.push(languages.shift())
        langControl.setAttribute('lang', languages[0])
        window.setTimeout(changeLocale, 5000)
    }

    // Handle DOM mutations
    function mutationHandler (mutationsList) {
        for (let mutation of mutationsList) {
            if (mutation.type === 'attributes') {
                newLocale = mutation.target.getAttribute(mutation.attributeName)
                localized = $('[data-locale]')
                localized.fadeOut(200, function (index) {
                    let elem = $(this)
                    textidx = elem.attr('data-locale')
                    elem.text(locales[newLocale][textidx])
                })
                localized.fadeIn(200)
            }
        }
    }

    async function getData (step) {
        let req = new Request('getProgressStatus.php?step=' + step)

        let res = await fetch(req).then(response => {
            return response.json()
        })

        if (res.skip) {
            location.href = '../nonet.php'
        }

        // Show step 2 if a device has connected to the AP.
        if (res.connected) {
            $('#connected-device').text(res.clientName + ' – ' + res.clientIp)
            $('#connected').show()
            $('#browser').show()
            $('#wifi').children().last().replaceWith('<hr>')
            step = 'accessed'
        }

        // Show step 2 if a device has accessed the setup page.
        if (res.accessed) {
            $('#follow').show()
            $('#browser').children().last().replaceWith('<hr>')
            step = 'completed'
        }

        // Redirect to network check after completion.
        if (res.completed) {
            setTimeout(() => {
                location.href = '../nonet.php'
            }, 5000)
        }

        // Re-trigger fetch after 5 seconds.
        setTimeout(getData, 5000, step)
    }

    let step = 'connected'
    getData(step)

    // Set up mutation observer and language rotation.
    const langNode = document.getElementById('language')
    let config = {attributes: true, childList: false}
    const observer = new MutationObserver(mutationHandler)
    observer.observe(langNode, config)

    let languages = ['en_GB', 'de_DE', 'ja_JP']
    window.setTimeout(changeLocale, 5000)

    <?php
    include('../config/glancrConfig.php');
    $localesAvailable = getAvailableLocales();
    $translations = [];

    foreach ($localesAvailable as $language) {
        setlocale(LC_MESSAGES, "$language.UTF-8");
        $translations[$language] = [
            '1-1' => _('or scan me'),
            '1-2' => _('I made a Wi-Fi for you.'),
            '1-3' => _('Connect your smartphone or laptop with me.'),
            '1-4' => _('password'),
            '1-5' => _('Connected'),
            '2-1' => _('On most devices, the setup screen should start automatically.'),
            '2-2' => _('If it doesn\'t, visit http://glancr.conf in your browser.'),
            '3-1' => _('Follow the instructions to complete the setup!')
        ];
    };
    ?>
    let locales = <?php echo json_encode($translations); ?>
</script>
</body>
</html>
