<?php

require '../config/glancrConfig.php';

$step = $_GET['step'];

switch ($step) {
    case "connected": // Test if a client has connected to the access point.

        $connections_file = file('../tmp/ap-clients.log', FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

        if ($connections_file === false) {
            // Break early if AP client connections log cannot be opened - most likely because it was deleted after a successful setup. Maybe we are already connected, send skip flag.
            $response = [
                'skip' => true
            ];
            break;
        }

        // If the file exists, get the last connection.
        $last_connection = array_pop($connections_file);

        if (!empty($last_connection)) {
            list($client, $ip) = explode(' ', $last_connection);

            $response = [
                'connected' => TRUE,
                'clientName' => $client,
                'clientIp' => $ip != 'UNKNOWN_IP' ? substr($ip, 1, -1) : $ip
            ];
        } else {
            $response = [
                'connected' => FALSE
            ];
        }
        break;

    case "accessed": // Test if client has accessed the config home page
        exec('sudo /usr/bin/tail -n 1 /var/log/apache2/glancr_conf_access.log', $output, $status);
        $response = [
            'accessed' => !empty($output) ? TRUE : FALSE
        ];
        break;

    case "completed": // Test if the setup was completed.
        $password = getConfigValue('wlanPass');
        $connection_type = getConfigValue('connectionType');
        $response = [
            'completed' => !empty($password) || $connection_type === 'eth' ? TRUE : FALSE
        ];
        break;
}

http_response_code(200);
print(json_encode($response));
